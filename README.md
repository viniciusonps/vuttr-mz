# MZ-challenge

O objetivo deste teste era desenvolver uma aplicação do zero e consumir uma API REST.
Nesta aplicação podemos criar, listar, remover e buscar cards, neste caso os cards serão usados para lembrarmos de ferramentas úteis do dia a dia.

## Tecnologias utilizadas

- React.JS;
- Styled Components;
  - Para a criação de componentes já estilizados;
  - Motivo: Pela facilidade de criação, manutenção e visualização de componentes já estilizados.
- Axios.
  - Para o consumo da API REST;
  - Motivos: Padrão muito utilizado no mercado, de fácil documentação e com interface "agradável";
- JSON-Server.
  - Para subir a API que me foi dada.

Utilizei Hooks, não senti necessidade de utilizar nenhum gerenciamento de estado global, decidi passar tudo por props.

## Estrutura das pastas

- src: Pasta principal;

  - assets: Pasta com ícones utilizados;

  - components: Pasta com componentes da aplicação;

    - Componente: Pasta que contém index do componente e os estilos utilizados por ele;
      - index.js e style.js utilizado pelo componente.

  - pages: Pasta que contém as páginas da aplicação;

    - page: Pasta que contém o index da página e os estilos utilizados pela página;
      - index.js e style.js utilizado pela página.

  - services: Pasta com principais funções compartilhadas pela aplicação;

    - api.js: Arquivo que cria o ponto de acesso a API REST;
    - tools.service.js: Arquivo que compartilha as funções da aplicação, estas funções fazem a captura das informações da API.

  - App.js: Principal arquivo da aplicação, recebe a página e a estilização global;
  - globalStyle.js: Arquivo com a estilização global;
  - index.js: Arquivo de entrada do React.

Decidi fazer essa organização das pastas de componentes e pages pois deixamos o Componente/Página e seu CSS lado a lado, não seria necessário buscar em outras pastas.
Também torna mais fácil na hora de fazer importações, pois você indica a página e ele já busca direto pelo arquivo "index.js".

## Wireframe e Protótipo

Me foram disponibilizados os 4 wireframes para que eu desenvolvesse esta aplicação.

Com base nestes wireframes eu desenvolvi um [protótipo](https://www.figma.com/file/WY5KA4XQJYPjVEuVsokB38/MZ-Challenge?node-id=0%3A1) utilizando o Figma.


## Como executar a aplicação

Primeiro será necessário subir o servidor da API.

1. Faça o clone/download deste repositório.
2. Acesse a pasta `/api`.
3. Execute o comando `npm install`.
4. Execute o comando `npx json-server db.json`;


OBS: A API deve ser inicializada na porta 3000.
Por exemplo: `http://localhost:3000`.

Agora para iniciarmos a aplicação:

1. Acesse a pasta `/mz-challenge`.
2. Execute o comando `yarn install`.
3. Execute o comando `yarn start`;

OBS: A aplicação em React.JS poderá ser inicializado em qualquer outra porta, fora a 3000.
Por exemplo: `http://localhost:3001`.

## Agradecimentos

Muito obrigado pela oportunidade.
