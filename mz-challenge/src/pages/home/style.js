import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
`;
export const Content = styled.div`
  width: 50%;
  padding: 3rem 0 0;
  margin: auto;

  display: flex;
  flex-direction: column;
`;

export const SearchAndButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 2.25rem;
`;
