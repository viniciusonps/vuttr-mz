import { useEffect, useState } from "react";
import Button from "../../components/Button";
import Card from "../../components/Card";
import Header from "../../components/Header";
import ModalAdd from "../../components/ModalAdd";
import ModalRemove from "../../components/ModalRemove";
import SearchBar from "../../components/SearchBar";
import { listTools, addTool, deleteTool } from "../../services/tools.service";
import * as S from "./style";

import addIcon from "../../assets/add.svg";

function Home() {
  const [tools, setTools] = useState([]);
  const [isOnlyTag, setIsOnlyTag] = useState(false);
  const [isModalAddOpen, setIsModalAddOpen] = useState(false);
  const [isModalRemoveOpen, setIsModalRemoveOpen] = useState(false);
  const [CardInfo, setCardInfo] = useState({});

  const handleIsOnlyTag = () => setIsOnlyTag(!isOnlyTag);

  const handleIsModalAddOpen = () => setIsModalAddOpen(!isModalAddOpen);

  const handleIsModalRemoveOpen = () =>
    setIsModalRemoveOpen(!isModalRemoveOpen);

  async function handleListTools(search) {
    let tools = await listTools(search, isOnlyTag);
    setTools(tools);
  }

  async function handleAddTool(title, link, description, tags) {
    await addTool(title, link, description, tags);
    setIsModalAddOpen(!isModalAddOpen);
    handleListTools();
  }

  async function handleDeleteTool(id) {
    await deleteTool(id);
    handleListTools();
  }

  const getCardInfo = () => CardInfo;

  const putCardInfo = (toolId, toolTitle) => {
    const tool = {
      id: toolId,
      title: toolTitle,
    };
    return setCardInfo(tool);
  };

  useEffect(() => {
    handleListTools();
  }, []);

  return (
    <S.Container>
      {isModalAddOpen && (
        <ModalAdd
          handleIsModalAddOpen={handleIsModalAddOpen}
          handleAddTool={handleAddTool}
        />
      )}
      {isModalRemoveOpen && (
        <ModalRemove
          handleIsModalRemoveOpen={handleIsModalRemoveOpen}
          handleDeleteTool={handleDeleteTool}
          getCardInfo={getCardInfo}
        />
      )}
      <S.Content>
        <Header />
        <S.SearchAndButtonContainer>
          <SearchBar
            handleListTools={handleListTools}
            handleIsOnlyTag={handleIsOnlyTag}
          />
          <Button
            title="Add"
            icon={addIcon}
            handleIsModalAddOpen={handleIsModalAddOpen}
          />
        </S.SearchAndButtonContainer>

        {tools.length > 0 &&
          tools.map((tool, index) => {
            return (
              <Card
                key={index}
                id={tool.id}
                title={tool.title}
                link={tool.link}
                description={tool.description}
                tags={tool.tags}
                handleIsModalRemoveOpen={handleIsModalRemoveOpen}
                putCardInfo={putCardInfo}
              />
            );
          })}
      </S.Content>
    </S.Container>
  );
}

export default Home;
