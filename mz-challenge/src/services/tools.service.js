import api from "./api";

const resource = "/tools";
const config = {
  headers: {
    "Content-Type": "application/json",
  },
};

async function listTools(queryValue, isOnlyTag) {
  let query = "";

  if (queryValue !== undefined) {
    query = isOnlyTag ? `?tags_like=${queryValue}` : `?q=${queryValue}`;
  }

  const { data } = await api.get(resource + query);
  return data;
}

async function addTool(title, link, description, tags) {
  const data = {
    title,
    description,
    link,
    tags,
  };
  const response = await api.post(resource, data, config);
  console.log(response.status);
  return response.status;
}

async function deleteTool(id) {
  const response = await api.delete(resource + `/${id}`);
  return console.log(response.status);
}

export { listTools, addTool, deleteTool };
