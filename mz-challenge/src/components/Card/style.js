import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  min-height: 150px;
  padding: 1.5rem;
  margin-bottom: 2.25rem;
  display: flex;
  flex-direction: column;

  background-color: #ffffff;
  border: none;
  border-radius: 10px;
  box-shadow: 0px 6px 6px 0px rgba(0, 0, 0, 0.08);
`;

export const CardHeader = styled.div`
  margin-bottom: 1.35rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Title = styled.h3`
  font-size: 1.25rem;
  font-weight: bold;

  transition: 0.2s;
  &&:hover {
    color: #1c1d28;
    text-decoration: underline;
  }
`;

export const Icon = styled.img`
  width: 0.7rem;
  margin-right: 0.5rem;
`;

export const RemoveButton = styled.button`
  width: 15%;
  height: 1.75rem;
  font-size: 0.9rem;
  font-weight: 500;

  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 5px;
  border: 2px solid #272937;

  background-color: transparent;
  cursor: pointer;

  transition: 0.2s;
  &&:hover {
    background-color: #f8f9fb;
    border-color: #1c1d28;
  }
`;

export const Description = styled.p`
  margin-bottom: 1.25rem;
  font-size: 0.9rem;
  font-weight: 300;
  color: #757893;
`;

export const Tags = styled.div`
  width: 100%;
  display: flex;
`;

export const Tag = styled.p`
  margin-right: 0.75rem;

  font-size: 0.9rem;
  font-weight: 800;
`;
