import * as S from "./style";
import cancelIcon from "../../assets/cancel.svg";

function Card({
  id,
  title,
  link,
  description,
  tags,
  handleIsModalRemoveOpen,
  putCardInfo,
}) {
  function handleRemoveTool(id, title) {
    putCardInfo(id, title);
    handleIsModalRemoveOpen();
  }

  return (
    <S.Container>
      <S.CardHeader>
        <a href={link}>
          <S.Title>{title}</S.Title>
        </a>
        <S.RemoveButton onClick={() => handleRemoveTool(id, title)}>
          <S.Icon src={cancelIcon} alt="Trash Icon" />
          Remove
        </S.RemoveButton>
      </S.CardHeader>

      <S.Description>{description}</S.Description>
      <S.Tags>
        {tags.map((tag, index) => (
          <S.Tag key={index}>#{tag}</S.Tag>
        ))}
      </S.Tags>
    </S.Container>
  );
}

export default Card;
