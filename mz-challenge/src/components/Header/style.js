import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-self: flex-start;
`;

export const Title = styled.h1`
  font-size: 3.5rem;
  margin-bottom: 1.5rem;
  font-weight: 800;
`;

export const Subtitle = styled.h2`
  font-size: 1.75rem;
  margin-bottom: 3rem;
  font-weight: 500;
`;
