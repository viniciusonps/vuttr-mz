import * as S from "./style";

const Header = () => {
  return (
    <S.Container>
      <S.Title>VUTTR</S.Title>
      <S.Subtitle>Very Useful Tools to Remember </S.Subtitle>
    </S.Container>
  );
};

export default Header;
