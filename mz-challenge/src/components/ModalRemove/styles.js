import styled from "styled-components";

export const Modal = styled.div`
  height: 100%;
  width: 100%;

  position: fixed;

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ModalBackground = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.25);
`;

export const ModalContent = styled.div`
  position: relative;

  height: 30%;
  width: 40%;

  border-radius: 10px;

  background: #f8f9fb;
`;

export const ContainerContent = styled.div`
  width: 80%;
  height: 100%;
  margin: 0 auto;
  padding: 2.5rem 0;

  display: flex;
  flex-direction: column;

  justify-content: space-between;
`;

export const Icon = styled.img`
  width: 0.9rem;
  margin-right: 0.5rem;
  align-self: center;
`;

export const Title = styled.h1`
  margin-bottom: 1.5rem;
  font-size: 1.25rem;
  font-weight: 500;
`;

export const CautionMessage = styled.p`
  margin-bottom: 1.5rem;
  color: #757893;

  strong {
    color: #272937;
  }
`;

export const ContainerButton = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const ConfirmButton = styled.button`
  width: 25%;
  height: 36px;
  margin-left: 1.5rem;
  display: flex;
  align-items: center;
  justify-content: center;

  border-radius: 5px;
  border: none;

  font-size: 0.8rem;
  font-weight: 500;
  color: #fff;

  cursor: pointer;

  background-color: #272937;
  transition: 0.2s;
  &&:hover {
    background-color: #1c1d28;
  }
`;

export const CancelButton = styled.button`
  width: 25%;
  height: 36px;

  display: flex;
  align-items: center;
  justify-content: center;

  background-color: transparent;

  border-radius: 5px;
  border: 2px solid #272937;
  cursor: pointer;

  font-size: 0.8rem;
  font-weight: 500;
  color: #272937;

  transition: 0.2s;
  &&:hover {
    background-color: white;
    border-color: #1c1d28;
  }
`;
