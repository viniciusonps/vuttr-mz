import { useEffect, useState } from "react";
import * as S from "./styles";
import cancelIcon from "../../assets/cancel.svg";

function ModalRemove({
  handleIsModalRemoveOpen,
  handleDeleteTool,
  getCardInfo,
}) {
  const [toolId, setToolId] = useState("");
  const [toolTitle, setToolTitle] = useState("");

  function handleRemoveTool(toolId) {
    handleDeleteTool(toolId);
    handleIsModalRemoveOpen();
  }

  function handleSetInfo() {
    const { id, title } = getCardInfo();
    // console.log(id, title);
    setToolId(id);
    setToolTitle(title);
  }

  useEffect(() => {
    handleSetInfo();
  }, []);

  return (
    <S.Modal>
      <S.ModalBackground onClick={handleIsModalRemoveOpen}></S.ModalBackground>
      <S.ModalContent>
        <S.ContainerContent>
          <S.Title>
            <S.Icon
              src={cancelIcon}
              alt="Close Icon"
              onClick={handleIsModalRemoveOpen}
            />
            Remove Tool
          </S.Title>
          <S.CautionMessage>
            Are you sure you want to remove "<strong>{toolTitle}</strong>"
          </S.CautionMessage>
          <S.ContainerButton>
            <S.CancelButton onClick={handleIsModalRemoveOpen}>
              Cancel
            </S.CancelButton>
            <S.ConfirmButton onClick={() => handleRemoveTool(toolId)}>
              Yes, remove!
            </S.ConfirmButton>
          </S.ContainerButton>
        </S.ContainerContent>
      </S.ModalContent>
    </S.Modal>
  );
}

export default ModalRemove;
