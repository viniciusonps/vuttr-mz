import * as S from "./style";

const Button = ({ title, handleIsModalAddOpen, icon }) => {
  return (
    <>
      <S.Button onClick={() => handleIsModalAddOpen()}>
        <S.Icon src={icon} />
        {title}
      </S.Button>
    </>
  );
};

export default Button;
