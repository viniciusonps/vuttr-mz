import styled from "styled-components";

export const Icon = styled.img`
  width: 0.75rem;
  margin-right: 0.5rem;
`;

export const Button = styled.button`
  width: 20%;
  height: 36px;

  display: flex;
  justify-content: center;
  align-items: center;

  border-radius: 5px;
  border: none;

  font-size: 0.9rem;
  font-weight: 500;
  color: #fff;

  cursor: pointer;

  background-color: #272937;

  transition: 0.2s;
  &&:hover {
    background-color: #1c1d28;
  }
`;
