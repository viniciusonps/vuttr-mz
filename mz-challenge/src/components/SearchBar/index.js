import * as S from "./style";
import searchIcon from "../../assets/search.svg";

const SearchBar = ({ handleListTools, handleIsOnlyTag }) => {
  return (
    <S.Container>
      <S.SearchContainer>
        <S.Icon src={searchIcon} />
        <S.SearchInput
          placeholder="search"
          type="text"
          onChange={(event) => handleListTools(event.target.value)}
        />
      </S.SearchContainer>

      <S.CheckboxInput type="checkbox" onClick={() => handleIsOnlyTag()} />
      <S.TagsText>search in tags only</S.TagsText>
    </S.Container>
  );
};

export default SearchBar;
