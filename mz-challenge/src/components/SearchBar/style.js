import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

export const SearchContainer = styled.div`
  width: 35%;
  padding: 0 0.75rem 0 1rem;
  margin-right: 0.75rem;
  display: flex;
  align-items: center;
  background-color: #f3f4f9;
  border-radius: 5px;

  box-shadow: 0px 6px 6px 0px rgba(0, 0, 0, 0.08);
`;

export const Icon = styled.img`
  width: 0.75rem;
  margin-right: 0.25rem;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 36px;
  padding: 0 0.25rem 0;
  border: none;
  font-weight: 300;
  background-color: #f3f4f9;

  ::placeholder {
    color: #c3c6db;
  }
`;

export const CheckboxInput = styled.input`
  height: 1.25rem;
  width: 1.25rem;
  margin-right: 0.75rem;
  cursor: pointer;
`;

export const TagsText = styled.label`
  font-size: 1rem;
  font-weight: 300;
  color: #757893;
`;
