import { useState } from "react";
import * as S from "./styles";
import addIcon from "../../assets/add.svg";
import addSolidIcon from "../../assets/add-solid.svg";

function ModalAdd({ handleIsModalAddOpen, handleAddTool }) {
  const [toolName, setToolName] = useState("");
  const [toolLink, setToolLink] = useState("");
  const [toolDescription, setToolDescription] = useState("");
  const [toolTags, setToolTags] = useState([]);

  const handleSaveName = (name) => {
    setToolName(name);
  };

  const handleSaveLink = (link) => {
    setToolLink(link);
  };

  const handleSaveDescription = (description) => {
    setToolDescription(description);
  };

  const handleSaveTags = (tags) => {
    let formatedTags = tags.split(/[, ;.  ]+/gm);
    setToolTags(formatedTags);
  };

  const verifyData = () => {
    if (!toolLink.includes("http")) {
      return setToolLink(`https://${toolLink}`);
    }
  };

  const handleCreateNewTool = () => {
    verifyData();
    return handleAddTool(toolName, toolLink, toolDescription, toolTags);
  };

  return (
    <S.Modal>
      <S.ModalBackground
        onClick={() => handleIsModalAddOpen()}
      ></S.ModalBackground>
      <S.ModalContent>
        <S.ContainerContent>
          <S.Title>
            <S.Icon src={addSolidIcon} alt="Add Icon" />
            Add new tool
          </S.Title>
          <S.Label>Tool name</S.Label>
          <S.Input
            placeholder="Enter with the tool name here..."
            type="text"
            onChange={(e) => handleSaveName(e.target.value)}
          />

          <S.Label>Tool link</S.Label>
          <S.Input
            placeholder="https://www.example.com"
            type="text"
            onChange={(e) => handleSaveLink(e.target.value)}
          />

          <S.Label>Tool description</S.Label>
          <S.InputDescription
            placeholder="Enter tool description here..."
            className="description"
            type="text"
            onChange={(e) => handleSaveDescription(e.target.value)}
          />

          <S.Label>Tags</S.Label>
          <S.Input
            placeholder="Enter the tags of the tool here...."
            type="text"
            onChange={(e) => handleSaveTags(e.target.value)}
          />

          <S.AddToolButton
            onClick={() => handleCreateNewTool()}
            alt="Add tool button"
          >
            <S.Icon src={addIcon} alt="Add Icon" />
            Add tool
          </S.AddToolButton>
        </S.ContainerContent>
      </S.ModalContent>
    </S.Modal>
  );
}

export default ModalAdd;
