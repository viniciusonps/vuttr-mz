import styled from "styled-components";

export const Modal = styled.div`
  height: 100%;
  width: 100%;

  position: fixed;

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ModalBackground = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, 0.25);
`;

export const ModalContent = styled.div`
  position: relative;
  height: auto;
  width: 25%;

  border-radius: 10px;

  background: #f8f9fb;
`;

export const ContainerContent = styled.div`
  width: 75%;
  height: 100%;
  margin: 0 auto;
  padding: 2.5rem 0;

  display: flex;
  flex-direction: column;
`;

export const Title = styled.p`
  margin-left: -1.3rem;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 1rem;
  font-size: 1.25rem;
  font-weight: 500;
`;

export const Label = styled.label`
  margin-top: 1.5rem;
  font-size: 0.9rem;
`;

export const Input = styled.input`
  height: 36px;
  margin-top: 1rem;
  padding: 0 1rem 0;

  background-color: #f3f4f9;
  border: none;
  border-radius: 5px;
  font-weight: 300;
  box-shadow: 0px 6px 6px 0px rgba(0, 0, 0, 0.08);

  ::placeholder {
    font-family: roboto, sans-serif;
    color: #c3c6db;
  }
  ::placeholder {
    font-family: roboto, sans-serif;
  }
`;

export const InputDescription = styled.textarea`
  height: 85px;
  margin-top: 1rem;

  padding: 1rem;
  background-color: #f3f4f9;
  border: none;
  border-radius: 5px;
  font-weight: 300;
  box-shadow: 0px 6px 6px 0px rgba(0, 0, 0, 0.08);

  resize: none;
  ::placeholder {
    font-family: roboto, sans-serif;
    color: #c3c6db;
  }
  ::placeholder {
    font-family: roboto, sans-serif;
  }
`;

export const Icon = styled.img`
  width: 0.8rem;
  margin-right: 0.5rem;
`;

export const AddToolButton = styled.button`
  width: 40%;
  height: 36px;

  display: flex;
  justify-content: center;
  align-items: center;
  align-self: flex-end;

  margin-top: 2rem;

  border-radius: 5px;
  border: none;

  font-size: 0.8rem;
  font-weight: 500;
  color: #fff;

  background-color: #272937;
  cursor: pointer;

  transition: 0.2s;
  &&:hover {
    background-color: #1c1d28;
  }
`;
